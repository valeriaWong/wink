import { defineComponent } from 'vue';

// Utils
import { createNamespace } from '../utils';

// Components
const [name, bem] = createNamespace('card');

export default defineComponent({
  name,
  props: {
    desc: String,
    title: String,
  },

  setup(props, { slots }) {
    return () => {
      return (
        <div class={bem()}>
          <div class={bem('header')}>
            <div class={bem('header') + "-title"}>
              {props.title}
            </div>
            <div class={bem('header') + "-desc"}>
              {props.desc}
            </div>
          </div>
        </div>
      );
    };
  },
});