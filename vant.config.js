module.exports = {
  name: 'wink',
  build: {
    namedExport: true,
    skipInstall: ['lazyload'],
    css: {
      preprocessor: 'less',
    },
    vetur: {
      tagPrefix: 'wink-'
    },
  },
  site: {
    title: 'wink',
    logo: 'https://img.yzcdn.cn/vant/logo.png',
    nav: [
      {
        title: '开发指南',
        items: [
          {
            path: 'home',
            title: '介绍',
          },
          {
            path: 'quickstart',
            title: '快速上手',
          },
        ],
      },
      {
        title: '基础组件',
        items: [
          {
            path: 'card',
            title: 'card 卡片',
          },
        ],
      },
    ],
  },
};
